<?php
class thumbnail_wizardAdminController extends thumbnail_wizard
{
	/**
	 * Initialization
	 *
	 * @return void
	 */
	function init()
	{
	}

	/**
	 * Save Settings
	 *
	 * @return mixed
	 */
	function procThumbnail_wizardAdminInsertConfig() {
		// Get the basic information
		//$args = Context::getRequestVars();

		$oModuleModel = &getModel('module');
		$thumbnail_wizard_config = $oModuleModel->getModuleConfig('thumbnail_wizard');
		$oModuleController = &getController('module');

		$thumbnail_wizard_config->use = Context::get('use');
		$thumbnail_wizard_config->user = Context::get('user');
		$thumbnail_wizard_config->pass = Context::get('pass');
		$thumbnail_wizard_config->maxsize = Context::get('maxsize');
		$thumbnail_wizard_config->thumbsize = Context::get('thumbsize');
		$thumbnail_wizard_config->delete = Context::get('delete');

		$thumbnail_wizard_config->mid = Context::get('mid');

		$oModuleController->insertModuleConfig('thumbnail_wizard', $thumbnail_wizard_config);
		$this->setMessage('success_updated');	
		//return output;
	}
}
/* End of file star_rating_config.admin.controller.php */
/* Location: ./modules/star_rating_config/star_rating_config.admin.controller.php */
